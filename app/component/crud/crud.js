const cassandra = require('cassandra-driver');

const client = new cassandra.Client({
    contactPoints: ['127.0.0.1'],
    localDataCenter: 'datacenter1',
    keyspace: 'chintal'
});

module.exports = {
    add: createEmployee,
    list: listEmployee,
    update: updateEmployee,
    delete: removeEmployee
}

function createEmployee(req, res) {
    console.log("create Employee", req.body)
    try {
        if (!req.body.id || !req.body.name) {
            console.log("1")
            res.json({ code: 500, message: "Please fill the  require  fields" })
        }

        var employee = {
            id: req.body.id,
            name: req.body.name
        };

        console.log("xyz", typeof employee.id)

        client.execute("INSERT INTO emp (id, name) VALUES ( " + employee.id + ", '" + employee.name + "')", [], function (err, data) {
            console.log("data", data)
            if (err) {
                console.log("2", err)
                res.json({ code: 500, data: {} })
            }
            else {
                res.json({ code: 200, message: "Contact created successfully" })
            }
        });

    } catch (error) {
        res.json({ code: 500, message: "catch block" })
    }
}

function listEmployee(req, res) {
    client.execute('SELECT * FROM emp', [], function (err, result) {
        if (err) {
            console.log('employees: list err:', err);

        } else {
            res.json({ message: 'employees: list succ:', data: result.rows })

        }
    });
}

function updateEmployee(req, res) {
    console.log("update employee", req.body)
    try {
        if (!req.body) {
            res.json({ code: 400, message: "Data to Update can not be empty" })
        }
        var employee = {
            id: req.body.id,
            name: req.body.name
        };

        client.execute("UPDATE emp set name = '" + employee.name + "' WHERE id = " + employee.id, [], function (err, data) {
            console.log("data", data)
            if (err) {
                console.log("2", err)
                res.json({ code: 500, data: {} })
            }
            else {
                res.json({ code: 200, message: "Employee update successfully" })
            }
        });

    } catch (error) {
        console.log("error", error)
        res.json({ code: 500, message: "catch block" })

    }
}

function removeEmployee(req, res) {
    console.log("delete employee", req.body)
    var id = req.params.id;
    client.execute("DELETE FROM emp WHERE id = " + id, [], function (err, data) {
        if (err) {
            console.log('employees: delete err:', err);
            res.json({ code: 500, data: {} })
        } else {
            console.log('employees: delete succ:');
            res.json({ code: 200, data: data })

        }
    });

}
