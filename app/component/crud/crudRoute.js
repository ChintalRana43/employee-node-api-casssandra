module.exports = function (router) {
    const crud = require('./crud')
    console.log("child")

    router.post('/createEmployee', crud.add)
    router.get('/listEmployees', crud.list)
    router.put('/updateEmployee/:id', crud.update)
    router.delete('/removeEmployee/:id', crud.delete)


    return router;
}